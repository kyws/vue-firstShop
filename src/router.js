import VueRouter from 'vue-router'
//导入对应的路由组件
import HomeContainer from "./components/tabbar/HomeContainer.vue"
import MemberContainer from "./components/tabbar/MemberContainer.vue"
import ShopcarContainer from "./components/tabbar/ShopcarContainer.vue"
import SearchContainer from "./components/tabbar/SearchContainer.vue"
import NewsList from "./components/news/NewsList.vue"
import NewsInfo from "./components/news/NewsInfo.vue"

//创建路由对象
var router = new VueRouter({
    routes: [
        //配置路由规则
        { path: "/", redirect: "/home" }, //这个叫路由重定向，如果是主页的话那我们重定向到houme
        { path: "/home", component: HomeContainer },
        { path: "/member", component: MemberContainer },
        { path: "/shopcar", component: ShopcarContainer },
        { path: "/search", component: SearchContainer },
        { path: "/home/newslist", component: NewsList },
        { path: "/home/newsinfo/:id", component: NewsInfo },
    ],
    //覆盖router-link标签的高亮类,router-link-active
    linkActiveClass: "mui-active"
})

// 把路由对象暴露出去
export default router