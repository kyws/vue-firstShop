//入口文件

//导入vue模块
import Vue from "vue"

//1.1导入路由的包
import VueRouter from "vue-router"
//1.2安装路由模块
Vue.use(VueRouter)
    //1.3导入自己的router.js模块
import router from "./router.js"

//2.1 导入vue-resource
import VueResource from "vue-resource"
//2.2 安装vue-resource
Vue.use(VueResource)
    //设置请求的根路径（咱后端的服务器）
Vue.http.options.root = 'http://vue.studyit.io';

import moment from "moment"
//定义全局的过滤器
Vue.filter("dataFormat", function(dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {
    return moment(dataStr).format(pattern)
})


//按需导入 Mint-UI 中的组件
import { Header } from "mint-ui"
Vue.component(Header.name, Header)

//导入轮播图组件
import { Swipe, SwipeItem } from 'mint-ui';
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);

//导入mui的样式
import "./lib/mui/css/mui.min.css"

//mui的字体图标样式
import "./lib/mui/css/icons-extra.css"

//导入app组件
import app from "./App.vue"

var vm = new Vue({
    el: "#app",
    //定义app组件
    render: c => c(app),
    //1.4挂载路由对象到vm 实例上
    router
})