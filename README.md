#第一个上手的vue项目.......模仿商城

## 制作首页app组件
1.完成header区域,使用的是mint-ui中的header组件
2.制作底部的tabbar区域，使用的是mui 的tabbar.html的代码片段
3.要在 中间区域放置 一个router-view 来展示路由匹配到的组件

##改造 tabbar 为 router-link
##设置路由高亮
##点击tabbar 创建对应的路由组件

##制作首页轮播图布局
##加载首页轮播图数据
1.获取数据，如何获取呢，使用vue-resource
2.使用 vue-resource 的this.$http.get 获取数据
3.获取到的数据要保存到 this.data身上
4.使用v-for 循环渲染 每个item对象

##改造九宫格样式

##改造新闻资讯 路由链接

##新闻资讯 页面制作
1.绘制界面
2.使用vue-resource 获取数据
3.渲染真是数据

##实现 新闻资讯列表 点击跳转到新闻详情
1.把列表中的每一项改造为router-link，同时在跳转的时候应该提供唯一的id标识符
2.创建新闻详情的组件 NewsInfo.vue
3.在路由模块中，将新闻详情的 路由地址 和组件页面对应起来